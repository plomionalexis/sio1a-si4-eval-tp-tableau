package evaltableaux;

public class E1_Q5 {
     
    public static void main(String[] args) {
        
          // Le tableau tempDec contient les températures relevées au mois de Décembre 2015
          // Le poste d'indice 0 contient la température du 1er Décembre ( 3°)
          // Le poste d'indice 1 contient la température du   2 Décembre ( 4°)
          // et ainsi de suite.        
          
          int[ ]   tempDec= {3, 4, 7, 5, -2, -3, 0, -4, -8, -8, -6, -3,  0, 4,  3,
                             2, 5, 4, 5,  6,  6, 5,  6,  6,  8, 10, 12, 10, 9, 10, 8};
       
            float somme = 0;
            float min = -8;
            float max = 12;
            
            
            for(int i=0; i < tempDec.length ; i++)
            {
            if(max < tempDec[i])
            {
                max=tempDec[i];
            }
            if(min > tempDec[i])
            {
                min=tempDec[i];
            }
            somme = somme + tempDec[i];
            }
            System.out.println("La temperature minimale du mois de décembre est : " +min);
            System.out.println("La température maximale du mois de décembre est : " +max);
          
    }  
}


