package evaltableaux;

public class E1_Q6 {
     
    public static void main(String[] args) {
        
          // Le tableau tempDec contient les températures relevées au mois de Décembre 2015
          // Le poste d'indice 0 contient la température du 1er Décembre ( 3°)
          // Le poste d'indice 1 contient la température du   2 Décembre ( 4°)
          // et ainsi de suite.        
          
          int[ ]   tempDec= {3, 4, 7, 5, -2, -3, 0, -4, -8, -8, -6, -3,  0, 4,  3,
                             2, 5, 4, 5,  6,  6, 5,  6,  6,  8, 10, 12, 10, 9, 10, 8};
       
       
        int tempMin=100;
        
        for (int i=0; i<tempDec.length; i++){
            if(tempDec[i]<tempMin){
                tempMin=tempDec[i];
               
            }
        }
        System.out.println("Température minimum du mois de décembre 2015: "+tempMin+"°");
        System.out.println("Cette température a été atteinte les jours suivants");
        for (int j=0; j<tempDec.length; j++){
            if (tempDec[j]==tempMin){
                System.out.println((j+1)+" Décembre 2015");
            }
        }
        
          
    }  
}


